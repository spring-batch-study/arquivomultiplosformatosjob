package com.springbatch.arquivomultiplosformatos.reader;

import com.springbatch.arquivomultiplosformatos.dominio.Cliente;
import com.springbatch.arquivomultiplosformatos.dominio.Transacao;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ClienteTransacaoLineMapper {

    /*
	ATENÇÃO!
	Não é usado tipagem em arquivos de múltiplos formatos.
	 */
    @Bean
    public PatternMatchingCompositeLineMapper lineMapper() {
        final PatternMatchingCompositeLineMapper lineMapper = new PatternMatchingCompositeLineMapper<>();
        lineMapper.setTokenizers(tokenizers());
        lineMapper.setFieldSetMappers(fieldSetMappers());
        return lineMapper;
    }

    private Map<String, FieldSetMapper> fieldSetMappers() {
        final Map<String, FieldSetMapper> fieldSetMapperMap = new HashMap<>();
        // toda linha que começa com 0 seguida de qualquer coisa, representa um cliente.
        fieldSetMapperMap.put("0*", fieldSetMapper(Cliente.class));

        // toda linha que começa com 1 seguida de qualquer coisa, representa uma transação.
        fieldSetMapperMap.put("1*", fieldSetMapper(Transacao.class));
        return fieldSetMapperMap;
    }

    private FieldSetMapper fieldSetMapper(Class clazz) {
        final BeanWrapperFieldSetMapper field = new BeanWrapperFieldSetMapper();
        field.setTargetType(clazz);
        return field;
    }

    private Map<String, LineTokenizer> tokenizers() {
        final Map<String, LineTokenizer> tokenizerMap = new HashMap<>();

        // toda linha que começa com 0 seguida de qualquer coisa, representa um cliente.
        tokenizerMap.put("0*", clienteLineTokenizer());

        // toda linha que começa com 1 seguida de qualquer coisa, representa uma transação.
        tokenizerMap.put("1*", transacaoLineTokenizer());
        return tokenizerMap;
    }

    private LineTokenizer transacaoLineTokenizer() {
        final DelimitedLineTokenizer transacaoLineTokenizer = new DelimitedLineTokenizer();
        transacaoLineTokenizer.setNames("id", "descricao", "valor");

        // pula-se o zero porque ele representa o identificador de tipo (cliente ou transação)
        transacaoLineTokenizer.setIncludedFields(1, 2, 3);
        return transacaoLineTokenizer;
    }

    private LineTokenizer clienteLineTokenizer() {
        final DelimitedLineTokenizer clienteLineTokenizer = new DelimitedLineTokenizer();
        clienteLineTokenizer.setNames("nome", "sobrenome", "idade", "email");

        // pula-se o zero porque ele representa o identificador de tipo (cliente ou transação)
        clienteLineTokenizer.setIncludedFields(1, 2, 3, 4);
        return clienteLineTokenizer;
    }

}
